Live build
==========

This repo is aimed to store my different buils of Debian, each one for a dedicated use :

* server-edition, the base used for my server (mainly web, but also git, mail, ...)
* my-cb for my custom version of the famous [Crunchbang](http://crunchbang.org/)
* cb-steam, a project of custom Crunchbang with multimedia stuffs like Steam (aka my custom Steam OS)

This project is based on the live build Debian tool (why the name of this repo) whose you can find some help on the net (http://crunchbang-fr.org/forum/viewtopic.php?id=1366 or http://yeuxdelibad.net/Logiciel-libre/Debian/Creer_sa_propre_distribution_avec_live-build.html, for example).

And for the full documenation, see [here](http://live.debian.net/manual/3.x/html/live-manual.en.html).
